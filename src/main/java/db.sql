DROP DATABASE IF EXISTS gestione_inventario;
CREATE DATABASE gestione_inventario;
USE gestione_inventario;

CREATE TABLE prodotto(
	prodottoID INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    codice VARCHAR(10) NOT NULL UNIQUE,
    nome VARCHAR(250) NOT NULL,
    descrizione VARCHAR(250) NOT NULL
);