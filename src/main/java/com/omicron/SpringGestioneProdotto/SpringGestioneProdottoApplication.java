package com.omicron.SpringGestioneProdotto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringGestioneProdottoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringGestioneProdottoApplication.class, args);
	}

}
