package com.omicron.SpringGestioneProdotto.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.omicron.SpringGestioneProdotto.models.Prodotto;
import com.omicron.SpringGestioneProdotto.services.ProdottoService;

@RestController
@RequestMapping("/prodotto")
public class ProdottoController {

	@Autowired
	private ProdottoService prodottoService;
	
	@GetMapping("/status/check")
	public String check() {
		return "UP";
	}
	
	@PostMapping("/inserisci")
	public Prodotto inserisciProdotto(@RequestBody Prodotto objProd) {
		return prodottoService.insert(objProd);
	}
	
	@GetMapping("/{prodotto_id}")
	public Prodotto cercaProdottoPerId(@PathVariable(name="prodotto_id") int prodId) {
		return prodottoService.findById(prodId);
	}
	
	@GetMapping("/")
	public List<Prodotto> cercaTuttiProdotti(){
		return prodottoService.findAll();
	}
	
	@DeleteMapping("/{prodotto_id}")
	public boolean eliminaProdotto(@PathVariable(name="prodotto_id") int prodId) {
		return prodottoService.delete(prodId);
	}
	
	@PutMapping("/modifica")
	public boolean modificaProdotto(@RequestBody Prodotto objProd) {
		return prodottoService.update(objProd);
	}
	
	
}
