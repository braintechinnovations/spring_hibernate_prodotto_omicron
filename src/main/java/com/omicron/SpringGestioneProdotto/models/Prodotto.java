package com.omicron.SpringGestioneProdotto.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="prodotto")
public class Prodotto {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="prodottoID")
	private int id;
	@Column
	private String codice;
	@Column
	private String nome;
	@Column
	private String descrizione;
	
	public Prodotto() {
		
	}
	
	public Prodotto(int id, String codice, String nome, String descrizione) {
		super();
		this.id = id;
		this.codice = codice;
		this.nome = nome;
		this.descrizione = descrizione;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCodice() {
		return codice;
	}
	public void setCodice(String codice) {
		this.codice = codice;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	@Override
	public String toString() {
		return "Prodotto [id=" + id + ", codice=" + codice + ", nome=" + nome + ", descrizione=" + descrizione + "]";
	}
	
	
	
}
